window.onload = function(){
    var head_li = document.querySelectorAll(".nav_head ul li");
    var body_li = document.querySelectorAll(".nav_body ul li");

    // 添加自定义属性
    for(let i=0;i<head_li.length;i++){
        head_li[i].setAttribute("index",i);
        body_li[i].setAttribute("index",i);
    }

    document.querySelector(".nav_head ul").addEventListener("click",function(e){
        var index = e.target.getAttribute("index")
        // 将其他所有的去掉选中效果
        for(let i=0;i<head_li.length;i++){
            head_li[i].className = "";
            body_li[i].style.display = "none"
        }
        e.target.className = "chose";
        body_li[index].style.display = "block"
        
    })
}